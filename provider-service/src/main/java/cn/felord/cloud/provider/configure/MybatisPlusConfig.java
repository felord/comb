package cn.felord.cloud.provider.configure;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * mybatis plus config
 * 加入 事务支持
 * 加入 缓存支持
 *
 * @author Dax
 * @since 14:27  2019-04-02
 */

@EnableTransactionManagement
public class MybatisPlusConfig {

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

}
