package cn.felord.cloud.provider.cache;

/**
 * 枚举超类
 *
 * @author Dax
 * @since 16 :17  2019/4/1
 */
public interface CacheEnumer {
    /**
     * Value int.
     *
     * @return int
     */
    String cacheName();

    /**
     * Description string.
     *
     * @return the string
     */
    int ttlSecond();
}
