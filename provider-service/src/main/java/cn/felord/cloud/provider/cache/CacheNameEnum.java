package cn.felord.cloud.provider.cache;


/**
 * 缓存枚举 缓存空间在此处声明.
 *
 * @author Dax
 */
public enum CacheNameEnum implements CacheEnumer {

    /**
     * 用户jwt token 缓存空间 ttl 7天  保持与配置文件一致同时调整  MultiWebSecurityConfig 中配置 REFRESH_TOKEN_EXP_DAYS
     */
    JWT_TOKEN_CACHE("usrTkn", 7 * 24 * 60 * 60),
    /**
     * 验证码缓存 5分钟ttl
     */
    SMS_CAPTCHA_CACHE("smsCode", 5 * 60),
    /**
     * sessionKey缓存时间 1天
     */
    SKY_CAPTCHA_CACHE("sky", 24 * 60 * 60),
    /**
     * 聊天好友关系缓存
     */
    CHAT_FRIEND_CACHE("chtFrd", 3 * 60),
    /**
     * 聊天用户
     */
    CHAT_USER_CACHE("chtUsrs", 10 * 10 * 60);


    /**
     * 缓存名称
     */
    private String cacheName;
    /**
     * 缓存过期秒数
     */
    private int ttlSecond;

    CacheNameEnum(String cacheName, int ttlSecond) {
        this.cacheName = cacheName;
        this.ttlSecond = ttlSecond;
    }


    @Override
    public String cacheName() {
        return this.cacheName;
    }

    @Override
    public int ttlSecond() {
        return this.ttlSecond;
    }
}
