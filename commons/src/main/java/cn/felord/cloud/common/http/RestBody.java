package cn.felord.cloud.common.http;

import lombok.Data;

/**
 * @author Dax
 * @since 14:34  2019-07-02
 */
@Data
class RestBody<T> implements Rest<T> {
    private static final long serialVersionUID = 76778342510613114L;

    private int statusCode;
    private T body;
    private String msg = "";
    private Object additional;

}
