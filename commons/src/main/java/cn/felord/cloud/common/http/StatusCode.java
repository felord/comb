package cn.felord.cloud.common.http;

import cn.felord.cloud.common.enumeration.Enumerate;

/**
 * The enum Status code.
 *
 * @author Dax
 * @since 15 :01  2019-07-02
 */
public enum StatusCode implements Enumerate {
    /**
     * Success status code.
     */
    SUCCESS(600, "success"),
    /**
     * Fail status code.
     */
    FAIL(700, "failure");


    private int code;
    private String description;

    StatusCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public int code() {
        return this.code;
    }

    @Override
    public String description() {
        return this.description;
    }
}
