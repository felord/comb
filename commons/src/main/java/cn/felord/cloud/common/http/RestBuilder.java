package cn.felord.cloud.common.http;

/**
 * The type Rest builder.
 *
 * @author Dax
 * @since 14 :46  2019-07-02
 */
public final class RestBuilder {

    private RestBuilder() {
    }

    /**
     * Ok rest.
     *
     * @param <T> the type parameter
     * @return the rest
     */
    public static <T> Rest<T> ok() {
        return okData(null, "", null);
    }

    /**
     * Ok rest.
     *
     * @param <T> the type parameter
     * @param msg the msg
     * @return the rest
     */
    public static <T> Rest<T> ok(String msg) {
        return okData(null, msg, null);
    }

    /**
     * Ok data rest.
     *
     * @param <T>  the type parameter
     * @param body the body
     * @return the rest
     */
    public static <T> Rest<T> okData(T body) {
        return okData(body, "", null);
    }

    /**
     * Ok data rest.
     *
     * @param <T>  the type parameter
     * @param body the body
     * @param msg  the msg
     * @return the rest
     */
    public static <T> Rest<T> okData(T body, String msg) {
        return okData(body, msg, null);
    }


    /**
     * Ok data rest.
     *
     * @param <T>        the type parameter
     * @param body       the body
     * @param msg        the msg
     * @param additional the additional
     * @return the rest
     */
    public static <T> Rest<T> okData(T body, String msg, Object additional) {
        return build(StatusCode.SUCCESS.code(), body, msg, additional);
    }


    /**
     * Fail rest.
     *
     * @param <T> the type parameter
     * @param msg the msg
     * @return the rest
     */
    public static <T> Rest<T> fail(String msg) {
        return fail(null, msg, null);
    }

    /**
     * Fail rest.
     *
     * @param <T>  the type parameter
     * @param body the body
     * @param msg  the msg
     * @return the rest
     */
    public static <T> Rest<T> fail(T body, String msg) {
        return fail(body, msg, null);
    }

    /**
     * Fail rest.
     *
     * @param <T>        the type parameter
     * @param body       the body
     * @param msg        the msg
     * @param additional the additional
     * @return the rest
     */
    public static <T> Rest<T> fail(T body, String msg, Object additional) {
        return build(StatusCode.FAIL.code(), body, msg, additional);
    }


    /**
     * Build rest.
     *
     * @param <T>        the type parameter
     * @param statusCode the status code
     * @param body       the body
     * @param msg        the msg
     * @param additional the additional
     * @return the rest
     */
    public static <T> Rest<T> build(int statusCode, T body, String msg, Object additional) {
        RestBody<T> restBody = new RestBody<>();

        restBody.setStatusCode(statusCode);
        restBody.setBody(body);
        restBody.setMsg(msg);
        restBody.setAdditional(additional);
        return restBody;
    }


}
