package cn.felord.cloud.common.http;

import java.io.Serializable;

/**
 * The interface Rest.
 *
 * @param <T> the type parameter
 * @author Dax
 * @since 14 :26  2019-07-02
 */
public interface Rest<T> extends Serializable {
    /**
     * Gets status code.
     *
     * @return the status code
     */
    int getStatusCode();

    /**
     * Sets status code.
     *
     * @param statusCode the status code
     */
    void setStatusCode(int statusCode);

    /**
     * Gets body.
     *
     * @return the body
     */
    T getBody();

    /**
     * Sets body.
     *
     * @param body the body
     */
    void setBody(T body);

    /**
     * Gets msg.
     *
     * @return the msg
     */
    String getMsg();


    /**
     * Sets msg.
     *
     * @param msg the msg
     */
    void setMsg(String msg);

    /**
     * Gets additional.
     *
     * @return the additional
     */
    Object getAdditional();

    /**
     * Sets additional.
     *
     * @param additional the additional
     */
    void setAdditional(Object additional);


}
