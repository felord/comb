package cn.felord.cloud.common.enumeration;

/**
 * The interface Enumerate.
 *
 * @author Dax
 * @since 15 :02  2019-07-02
 */
public interface Enumerate {

    /**
     * Code int.
     *
     * @return the int
     */
    int code();

    /**
     * Description string.
     *
     * @return the string
     */
    String description();
}
